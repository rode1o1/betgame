using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BetGame : MonoBehaviour
{
    public InputField inputField;
    public Text multiplierText;
    public Text timerText;
    public Button betButton;
    public Button withdrawButton;
    public Button playAgainButton;

    private float currentBet;
    private float currentMultiplier = 1.0f;
    private float timer = 0.0f;
    private bool bombExploded = false;

    private float timeToNextExplosion = 1.0f;
    private float explosionTimer = 0.0f;
    private int consecutiveSameNumbers = 0;
    private float lastRandomNumber = -1f;
    private bool betPlaced = false;
    private bool withdrew = false;
    public Text resultText;
    private float winnings;
    public ParticleSystem explosionParticles;
    public AudioSource explosionSound;

    private void Start()
    {
        betButton.onClick.AddListener(Bet);
        withdrawButton.onClick.AddListener(Withdraw);
        playAgainButton.onClick.AddListener(PlayAgain);
    }

    private void Update()
    {
        if (!bombExploded && betPlaced)
        {
            timer += Time.deltaTime;
            currentMultiplier = 1.0f + timer * 0.2f;
            multiplierText.text = "Multiplier: " + currentMultiplier.ToString("F2");
            timerText.text = "Timer: " + timer.ToString("F2");

            explosionTimer += Time.deltaTime;

             if (explosionTimer >= timeToNextExplosion){
               float newRandomNumber = Random.Range(0, 10);
               Debug.Log(newRandomNumber);
               if (newRandomNumber == lastRandomNumber){
                consecutiveSameNumbers++;
                Debug.Log(consecutiveSameNumbers);
                
                if (consecutiveSameNumbers >= 2)
                    {
                        ExplodeBomb();
                    }
               }
               else{
                  consecutiveSameNumbers = 1;
               }
                lastRandomNumber = newRandomNumber;
                explosionTimer = 0.0f;
             }
        }
    }

    private void Bet()
    {
        if(string.IsNullOrEmpty(inputField.text)){
            return;
        }
        float.TryParse(inputField.text, out currentBet);
        inputField.interactable = false;
        betButton.interactable = false;
        withdrawButton.interactable = true;
        betPlaced = true;
    }

    private void Withdraw()
    {
        winnings = currentBet * currentMultiplier;
        Debug.Log("Winnings: " + winnings);
        withdrew = true;
        playAgainButton.interactable = true;
        withdrawButton.interactable = false;
    }

    private void PlayAgain()
    {
        inputField.interactable = true;
        betButton.interactable = true;
        withdrawButton.interactable = false;
        playAgainButton.interactable = false;
        currentBet = 0.0f;
        currentMultiplier = 1.0f;
        timer = 0.0f;
        bombExploded = false;
        multiplierText.text = "Multiplier: 1.00";
        timerText.text = "Timer: 0.00";
        resultText.text = "";
        betPlaced = false;
        withdrew = false;
        explosionParticles.Stop();
    }

        private void ExplodeBomb()
    {
        if (withdrew){
            resultText.text = "You won: " + winnings.ToString("F2");
        }
        else{
            resultText.text = "You Lose" ;
             withdrawButton.interactable = false;
        }
       
        Debug.Log("BOOM! The bomb exploded!");
        bombExploded = true;
        timerText.text = "BOMB EXPLODED!";
        playAgainButton.interactable = true;
        betPlaced = false;
        explosionParticles.Play();
         explosionSound.Play();
    }
}
